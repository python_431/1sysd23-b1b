#include<stdio.h>

int main() {
    int sizes[] = { 12, 18, 22 };
    int dblsizes[3];

    for (int i = 0; i < 3; i++) {
        dblsizes[i] = sizes[i] * 2;
    }

    for (int i = 0; i < 3; i++) {
        printf("%d %d %d\n", i, sizes[i], dblsizes[i]);
    }
}
