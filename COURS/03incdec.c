#include<stdio.h>

int main() {
    int i = 42;

    printf("valeur de i : %d\n", i);
    i++;
    printf("valeur de i : %d\n", i);
    ++i;
    printf("valeur de i : %d\n", i);

    i = 42;
    printf("valeur de i : %d\n", i);
    printf("valeur de i++ : %d\n", i++);
    printf("valeur de i : %d\n", i);
    printf("valeur de ++i : %d\n", ++i);
    printf("valeur de i : %d\n", i);
}
