#include<stdio.h>

int main() {
    int x = 42, y;

    y = 12;
    printf("Bonjour le monde !\n");
    // %d -> affiche un "int" à cette endroit
    printf("%d coin coin %d\n", x, x+y);
    // adresse de x (entier "l"ong : 64bits)
    printf("adresse mémoire de x : %ld\n", &x);
    // adresse de x en hexadécimal
    printf("adresse mémoire de x : %lx\n", &x);
}
