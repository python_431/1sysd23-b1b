# Révisions

## Fonctions, float et switch

Comment demander la saisie d'un caractère avec `scanf` :

~~~~C
char c; // code du caractère sur 8 bits
...
scanf(" %c", &c); // notez bien : espace avant le %c
...
if (c == 'a') {  // ou c == 97 (norme ASCII)
    ...
}
~~~~

Pour saisir un nombre float (ou double) : `scanf("%f", &x);` pour
l'affichage `printf("%f", x);` et on peut fixer un format pour
la gestion du nombre de chiffre avant et après la virgule :
`printf("%.2f", x)` : 2 chiffres (décimaux) après la virgule.
(`man 3 printf`).

Écrire deux fonctions `float euro2franc(float val)` et `float franc2euro(float val)` qui convertissent euros en francs (français) et inversement. Le taux de conversion est de 6,55957 francs pour un euro.

Dans la fonction `main` écrire le code pour que le programme se comporte
ainsi :

~~~~C
$ ./euros
(1) Franc vers Euro ou (2) Euro vers franc ? 3
(1) Franc vers Euro ou (2) Euro vers franc ? a
(1) Franc vers Euro ou (2) Euro vers franc ? 1
Valeur en Franc : 100
En Euro : 15.24
$ ./euros
(1) Franc vers Euro ou (2) Euro vers franc ? 2
Valeur en Euro : 15
En Francs : 98.39
$
~~~~

## Quelle est la "qualité" du hasard de srand/rand ?

Voir `game.c` pour retrouver comment tirer un nombre entier
au hasard.

Consignes en commentaire du fichier `testrandom.c`.

## Tableau à plus d'une dimension 

On peut créer des tableaux multi-dimensionnels :

~~~~C
double matrix1[3][3] = { { 1, 2, 3}, {3, 4, 5}, {6, 7, 8} };
double matrix2[3][3];
~~~~

À partir de `linalg.c` implémenter des fonctions :
`void add(double m1[3][3], double m2[3][3], result[3][3])`, 
`void mult(...)` et `void transp(double m[3][3], double result[3][3])`.

On devrait avec mon code de test avoir comme sortie :

~~~~
1.00 2.00 3.00
4.00 5.00 6.00
7.00 8.00 9.00

2.00 3.00 4.00
5.00 6.00 7.00
8.00 9.00 10.00
Addition
3.00 5.00 7.00
9.00 11.00 13.00
15.00 17.00 19.00
Produit
36.00 42.00 48.00
81.00 96.00 111.00
126.00 150.00 174.00
Transposée
1.00 4.00 7.00
2.00 5.00 8.00
3.00 6.00 9.00
~~~~
