#include<stdio.h>

// arg !!! % n'est pas le "vrai" modulo mathématique !
// cf. https://stackoverflow.com/questions/11720656/modulo-operation-with-negative-numbers

int mod(int a, int b) {
    int r = a % b;
    if ( r < 0 ) {
        return r + b;
    } else {
        return r;
    }
    //return r < 0 ? r + b : r;
}

void cesar(char s[], int k) {
    char start;
    while (*s) {
        if ( *s >= 'A' && *s <= 'Z') {
            start = 'A';
        } else if ( *s >= 'a' && *s <= 'z' ) {
            start = 'a';
        } else {
            start = 0; 
        }
        if (start) { // faux si non alphabétique
            *s = mod(*s - start + k, 26) + start;
        }
        s++;
    }
}

// déchiffrement
void rasec(char *s, int k) {
    cesar(s, -k);
}

int main() {
    char phrase[1000]; // 999 caractères max
    int secret;

    printf("Tapez une phrase : ");
    scanf("%[^\n]", phrase); // permet d'avoir des espaces dans la donnée
    getchar(); // consomme le saut de ligne
    printf("Décalage (secret) : ");
    scanf("%d", &secret);
    cesar(phrase, secret);
    printf("Phrase chiffrée : %s\n", phrase);
    rasec(phrase, secret);
    printf("Phrase déchiffrée : %s\n", phrase);
}
