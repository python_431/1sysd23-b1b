#include<stdio.h>
#include<string.h>
#include<stdlib.h>

// arg !!! % n'est pas le "vrai" modulo mathématique !
// cf. https://stackoverflow.com/questions/11720656/modulo-operation-with-negative-numbers

int mod(int a, int b) {
    int r = a % b;
    if ( r < 0 ) {
        return r + b;
    } else {
        return r;
    }
    // pareil que : return r < 0 ? r + b : r;
}


// version avec renvoi d'une nouvelle chaîne
// au lieu d'une modification "en place"

char *cesar(char s[], int k) {
    char *crypted;
    char start, *p;
    crypted = malloc(sizeof(char)*(strlen(s) + 1));
    p = crypted;
    while (*s) {
        if ( *s >= 'A' && *s <= 'Z') {
            start = 'A';
        } else if ( *s >= 'a' && *s <= 'z' ) {
            start = 'a';
        } else {
            start = 0; 
        }
        if (start) { // faux si non alphabétique
            *p = mod(*s - start + k, 26) + start;
        } else {
            *p = *s;
        }
        s++;
        p++;
    }
    return crypted;
}

// déchiffrement
char *rasec(char *s, int k) {
    return cesar(s, -k);
}

void brute_force(char *crypted, char *hint) {
    char *try;
    for (int k = 1; k < 26; k++) { // de 1 à 25
        try = rasec(crypted, k);
        if (strstr(try, hint)) { // indice trouvé
            printf("Trouvé ! k = %d\n", k);
            printf("%s\n", try);
            free(try);
            break;
        } else {
            free(try);
        }
    }
}

int main() {
    char phrase[100]; // 99 caractères max
    char *p;
    int secret;
    char mystery[] = "Z UF EFK CZBV JGRD RK RCC!!! Z UF EFK NREK KYRK! A'VJGVIV HLV MFLJ MFLJ VKVJ RDLJVJ GVEUREK TV TFLIJ...";
    char hint[] = "SPAM";

   /*  printf("Tapez une phrase : ");
    scanf("%[^\n]", phrase); // permet d'avoir des espaces dans la donnée
    getchar(); // consomme le saut de ligne
    printf("Décalage (secret) : ");
    scanf("%d", &secret);
    p = cesar(phrase, secret);
    printf("Phrase chiffrée : %s\n", p);
    p = rasec(p, secret);
    printf("Phrase déchiffrée : %s\n", p);
   */
    brute_force(mystery, hint);
}
