#include<stdio.h>

int main() {
    int tab[] = { 12, 16, 42, -1, 24 };
    int i;
    int *p;

    p = tab;

    i = 2;
    printf("%d\n", tab[i]); // 42
    printf("%d\n", p[i]); // 42
    printf("%lx %lx\n", p, p + i); 
    printf("%d %d\n", *p, *(p + i));
    printf("%lx\n", i + p); // habituellement + est commutatif
    printf("%d\n", *(i + p)); // habituellement + est commutatif
    // WAT ? DON'T DO THAT !!!
    printf("%d %d\n", i[p], i[tab]);
}
