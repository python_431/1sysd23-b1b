# Chaînes de caractères

Le type `char` est, en réalité, un entier (signé) sur 8 bits. On
l'interprète comme le _code_ ASCII d'un caractère. Par exemple
'A' et 65 sont _la même chose_.

Une chaîne de caractère en C est un tableau de _char_, la fin
de la chaîne est indiqué par la valeur 0 (qu'on peut écrire aussi
'\0' mais surtout pas '0' (qui vaut... 48 !!!).

Dans l'exemple ci-dessous s1, s2 et s3 sont trois tableaux
strictement _identiques_ stockant des chaînes de caractères.

~~~~C
#include<stdio.h>

int main() {
    char s1[] = { 'H', 'e', 'l', 'l', 'o', '\0' }; 
    char s2[] = { 72, 101, 108, 108, 111, 0 };
    char s3[] = "Hello"; // le zéro terminal est ajouté automatiquement

    printf("%s\n", s1);
    printf("%s\n", s2);
    printf("%s\n", s3);
}
~~~~

_Note_ : voir `str1.c` pour un exemple de parcours de chaîne caractère
par caractère.

Pour lire une chaîne saisie au clavier avec scanf on peut faire :
`scanf("%s", str)` (note : pas de `&`, str (de type char[]) est
déjà une adresse mémoire !)

Si on souhaite permettre la présence d'espaces dans la chaîne (sinon
`scanf` ne garde que le premier mot : `scanf("%[^\n]", str);`. Il
faut alors consommer le retour chariot qui est encore dans le tampon
_(buffer)_ d'entrée : `getchar();`.

0. Écrire un programme C qui affiche tous les caractères ASCII
   affichables (code de 32 à 127)
1. Écrire une fonction `slen` qui calcule et renvoie la longueur d'une chaîne
2. Écrire une fonction `sequal` qui renvoie 0 si deux chaînes sont
différentes et une valeur non nulle si elle sont identiques

## Arithmétique de pointeurs et chaîne

Si p est un pointeur vers un élément d'un tableau, p+1 pointe
sur l'élément suivant dans le tableau, p++ permet d'avancer
dans le tableau, `*p++` : lit l'élément courant et fait avancer
p dans le tableau.

Un exemple : calcul de longeur :

~~~~C 
int slen(char s[]) {
    char *p;
    int l = 0;
    p = s;
    while (*p++) { // pareil != '\0'
        l++;
    }
    return l;
}
~~~~

Exercice : réécrire `sequal` en utilisant non pas un indice `i`
mais deux pointeurs `p` et `q` (type `char *`) qui pointent
respectivement vers le caractère courant de chaque chaîne.

Implémenter toutes les fonctions du transparent 50, (slen et
sequal sont déjà fournis dans `str1.c`) en utilisant systématiquement
des pointeurs.

Bonus : écrire une fonction qui renvoie vrai si une chaîne est
un palindrome (de façon efficace) avec des indices ou des pointeurs.

## Cryptographie

Code de César (cf. Guerre des Gaules) : décalage circulaire sur
l'alphabet (cf `cesar.c`).

Si le "secret" est 5 :

~~~~
A -> F
B -> G
C -> H
D -> I
...
U -> Z
V -> A
W -> B
X -> C
Y -> D
Z -> E
~~~~

1. Étant donné un caractère de code ascii _c_ et un "secret" k
(décalage), quel est le code ascii du caractère chiffr par _k_ ?

*Attention* : l'opérateur '%' n'est pas vraiment le modulo habituel,
ce qu'il est en Python... En particulier il n'a pas le "bon" comportement
quand l'opérande de gauche est négatif !

Cf. https://stackoverflow.com/questions/11720656/modulo-operation-with-negative-numbers

Une fonction qui fait ce que l'on veut ici est :

~~~~C
int mod(int a, int b) {
    int r = a % b;
    return r < 0 ? r + b : r;
}
~~~~

ou (sans utiliser l'opérateur `?` dit _ternaire_ que nous n'avons
pas vu) ce qui est équivalent :

~~~~C
int mod(int a, int b) {
    int r = a % b;
    if ( r < 0 ) {
        return r + b;
    } else {
        return r;
    }
    //return r < 0 ? r + b : r;
}
~~~~

2. Écrire la fonction qui chiffre une chaîne de caractère
   `void cesar(char s[], int k)` (en modifiant s)
   (la fonction devrait marcher aussi bien avec des capitales
   que des minuscules et laisser inchangé les caractères
   non-alphabétiques)
3. Comment écrire simplement la fonction de déchiffrement ?
 Écrivez là

### Deuxième partie : déchiffrement avec indice

1. Nous avons capturé un messager romain avec ce texte sur une
tablette en marbre : 

~~~~
Z UF EFK CZBV JGRD RK RCC!!! Z UF EFK NREK KYRK!
A'VJGVIV HLV MFLJ MFLJ VKVJ RDLJVJ GVEUREK TV TFLIJ...
~~~~

Un espion nous a indiqué le le mot _SPAM_ est présent dans
le texte en clair. Écrire une fonction qui utilise ce genre
d'information : `void brute_force(char crypted[], char hint)`
qui utilise un indice _(hint)_ pour trouver (si possible)
le texte en clair en testant tous les codes possibles.
Info : il existe une fonction de la bibliothèque C permettant
de savoir si une sous-chaîne est présente dans une chaîne
(`strstr`).

Pour tester successivement tous les déchiffrements possibles
on va devoir garder la chaîne d'origine sous la main. Soit
on la copie (strcpy/malloc ou tableau avec une valeur max)
dans les fonction de chiffrement/déchiffrement (donc on change
leur prototype : `char *cesar(char *s, int k);`) ou bien
dans la fonction `brute_force`. Plutôt le premier choix. 
(pensez à regarder dans la bibliothèque C : `strlen`, `strcpy`).

