#include<stdio.h>

// ces deux fonctions sont équivalentes !

int slen(char s[]) {
    int l = 0;
    while( s[l] != '\0' ) {
        l++;
    }
    return l;
}

int slen2(char s[]) {
    int l = 0;
    while( s[l] ) {
        l++;
    }
    return l;
}

int slen3(char s[]) {
    char *p;
    int l = 0;
    p = s;
    while (*p++) { // pareil != '\0'
        l++;
    }
    return l;
}


int sequal(char s1[], char s2[]) {
    int i = 0;

    while( s1[i] && s2[i] && s1[i] == s2[i] ) {
        i++;
    }
    return s1[i] == s2[i];
}

int sequal2(char s1[], char s2[]) {
    char *p, *q;

    // for ( p = s1, q = s2; *p && *q && *p == *q; p++, q++)
    // je trouve la version avec while plus lisible !
    p = s1;
    q = s2;
    while ( *p && *q && *p == *q ) {
        p++;
        q++;
    }
    return *p == *q;
}

int main() {
    int i;

    char s1[] = { 'H', 'e', 'l', 'l', 'o', '\0' }; 
    char s2[] = { 72, 101, 108, 108, 111, 0 };
    char s3[] = "Hello"; // le zéro terminal est ajouté automatiquement
    char str1[51], str2[51]; // taille max 50 ! 

    printf("%s\n", s1);
    printf("%s\n", s2);
    printf("%s\n", s3);

    i = 0;
    while(s3[i]) { // 0 (fin de chaîne) est faux
        printf("%c\n", s3[i]);
        i++;
    }
    printf("%d\n", slen(s3));
    printf("%d\n", slen2(s3));
    printf("%d\n", slen3(s3));
   
    printf("Chaine 1 : ");
    scanf("%[^\n]", str1); 
    getchar(); // consomme le \n
    printf("Chaine 2 : ");
    scanf("%[^\n]", str2); 
    getchar(); // consomme le \n
    printf("Égales (sequal)  ? %d\n", sequal(str1, str2));
    printf("Égales (sequal2) ? %d\n", sequal2(str1, str2));

}
