#include<stdio.h>

// % n'est pas le modulo habituel (comme en Python !)

// le "bon" modulo pour le code de César :
int mod(int a, int b) {
    int r = a % b;
    if ( r < 0 ) {
        return r + b;
    } else {
        return r;
    }
    //return r < 0 ? r + b : r;
}

int main() {
    printf("-4 % 26 = %d\n", -4 % 26);
    printf("mod(-4, 26) = %d\n", mod(-4, 26));
}
