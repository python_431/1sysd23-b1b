#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<unistd.h>

#define LINES 20 
#define COLS  20 

// pour le calcul du voisinage dans un tore
// (identification des côtés opposés)
int mod(int a, int b) {
    int r = a % b;
    if ( r < 0 ) {
        return r + b;
    } else {
        return r;
    }
    // pareil que : return r < 0 ? r + b : r;
}


void init_world(char world[LINES][COLS]) {
    for (int i = 0; i < LINES; i++) {
        for (int j = 0; j < COLS; j++) {
            if (rand() % 2 == 0) {
                world[i][j] = ' ';
            } else {
                world[i][j] = '*';
            }
        }
    }
}

void display_world(char world[LINES][COLS]) {
    for (int i = 0; i < LINES; i++) {
        for (int j = 0; j < COLS; j++) {
            printf("%c", world[i][j]);
        }
        printf("\n");
    }
}

// avec rien après les bords du monde...
int neighbors1(char world[LINES][COLS], int x, int y) {
    int n = 0;

    for (int i = y - 1; i <= y + 1; i++) {
        for (int j = x - 1; j <= x + 1; j++) {
            if ( i >= 0 && j >= 0 &&         // pas en dehors à gauche/haut
                 i < LINES && j < COLS &&    // pas en dehors à droite/bas
                 !(i == y && j == x ) &&     // pas sur le centre
                 world[i][j] == '*' ) {      // et vivant !
                    // printf("Voisin : %d, %d : %c\n", i, j, world[i][j]);
                    n++;
            }
        }
    }
    // printf("n = %d\n", n);
    return n;
}

// avec un monde toroïdal
int neighbors2(char world[LINES][COLS], int x, int y) {
    int n = 0;

    for (int i = y - 1; i <= y + 1; i++) {
        for (int j = x - 1; j <= x + 1; j++) {
            if (!(i == y && j == x ) && world[mod(i, LINES)][mod(j, COLS)] == '*') {
                n++;
            }
        }
    }
    return n;
}

void update_world(char world[LINES][COLS]) {
    char new_world[LINES][COLS];
    int neigh;

    for (int i = 0; i < LINES; i++) {
        for (int j = 0; j < COLS; j++) {
            neigh = neighbors1(world, i, j);   
            if (world[i][j] == '*') { // vivant
                if (neigh == 2 || neigh == 3) { // survie
                    new_world[i][j] = '*';
                } else {
                    new_world[i][j] = ' '; // meurt
                }
            } else { // mort
                if (neigh == 3) { // naît
                    new_world[i][j] = '*';
                } else {
                    new_world[i][j] = ' ';
                }
            }
        }
    }
    // copie
    for (int i = 0; i < LINES; i++) {
        for (int j = 0; j < COLS; j++) {
            world[i][j] = new_world[i][j];
        }
    }
}


int main() {
    char world[LINES][COLS];

    srand(time(NULL));

    init_world(world);
    while (1) { // ctrl-C pour quitter
        display_world(world);
        sleep(1);
        update_world(world);
        system("clear"); // efface le terminal (commande UNIX)
    }
}
