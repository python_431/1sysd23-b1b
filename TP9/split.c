#include<stdio.h>
#include<stdlib.h>

int split(char *msg, char *words[]) {
    char *p, *start;
    int n = 0;
    start = p = msg;
    // si la chaîne est vide il n'y a rien à faire ...
    if ( *msg == '\0' ) {
        return 0;
    }
    while (1) { // penser à faire break si *p == '\0' !
        // avancer jusqu'à l'espace ou \0 suivant
        while (*p != ' ' && *p != '\0') {
            p++;
        }
        // copier de start à p dans words[n], ajouter le 0
        // terminal, augmenter n
        words[n] = malloc((p - start + 1)*sizeof(char));
        for (int i = 0; i < p - start; i++) {
            // ou encore : *(words[n] + i) = *(start + i);
            words[n][i] = start[i];
        }
        words[n][ p - start ] = '\0';
        n++;
        // on a atteint la fin de msg ? si oui break, sinon on avance
        if (*p == '\0') {
            break;
        } else {
            p++;
            start = p; 
        }
    }
    return n;
}

void usage(char *argv[]) {
    printf("Usage : %s \"to be or not to be\"\n", argv[0]);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
    char *words[50]; // 50 mots maximum
    int n;
    if (argc != 2) {
        usage(argv);
    }
   
    n = split(argv[1], words);
    printf("%d mots :\n", n);
    for (int i = 0; i < n; i++) {
        printf("%s\n", words[i]);
    }
    exit(EXIT_SUCCESS);
}
