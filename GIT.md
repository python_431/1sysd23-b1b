# Créer et publiquer un dépôt (projet) GIT

## Générer une paire de clés (privée/publique) SSH

~~~~Bash
$ ssh-keygen -t rsa
~~~~

Répondez par [entrée] à toutes les questions !
(ne spécifiez pas de phrase de passe)

~~~~Bash
$ ls ~/.ssh 
id_rsa id_rsa.pub
$ cat ~/.ssh/id_rsa.pub
ssh-rsa AAAA.....
    WD5DFGH votre_login@votre_machine
~~~~

## Créez un compte (sauf si vous en avez un) sur Gitlab ou Github

Une fois connecté avec un navigateur sur Gitlab ou Github créez
un nouveau projet *public* nommé `1sysd`.

Copiez le _contenu_ du fichier `id_rsa.pub` et copiez-collez
ce contenu dans une clef SSH définit dans votre profil Gitlab.

Chez vous (répertoire personnel `~`) dans le répertoire
`.ssh`  

~~~~Bash
$ cat ~/.ssh/id_rsa.pub
ssh-rsa plein de truc
encore plein de trucs login@machine
$
~~~~

## Clonez votre projet sur votre système

Sur l'interface Web de Gitlab allez sur votre projet.
Bouton "Code", on voit deux url :

- une en `git@...` : copiez la dans le presse papier
- une en `https://...` : copiez la dans le chat Wimi

Dans un terminal lancez la commande de clonage :

~~~~Bash
$ sudo apt install git
$ git config --global user.name "Votre prénom"
$ git config --global user.email "votre email"
$ git clone git@...(lien git de votre projet)1sysd.git
$ ls 
... 1sysd ...
$ cd 1sysd
$ cp ../hello.c .
$ cp ../Makefile .
$ git add hello.c Makefile
$ git commit -a -m 'début du projet'
$ git push
~~~~

Retournez sur la page Web de votre dépôt, vous devriez y 
voir la présence des fichiers `hello.c` et `Makefile`.

Base de git :

- `git add fichier(s) ...` : indique qu'un fichier doit être "suivi"
  par git
- `git commit a -m "commentaire"` : enregistre l'état après un ajout
  de fichiers à suivre et/ou modifications de fichiers suivis
- `git push` : envoie l'état du projet sur la plateform Gitlab

Cycle de vie d'un exercice ?

1. Créer un répertoire dans votre répertoire `1sysd`

~~~~Bash
$ pwd
/home/votre_id/1sysd/
$ mkdir TP0
$ cd TP0
.... crée Makefile, des fichiers sources c
$ git add Makefile fichier1.c fichier2.c ...
$ make
... ça marche ?
$ ./monprgramme
... ça marche ?
$ git commit -a -m "exo fini" 
$ git push
~~~~

Clonez mon dépôt git (pas dans le vôtre !!!)

~~~~
$ cd
$ pwd
/home/votre_id
vérifiez bien que vous êtes chez vous et pas dans 1sysd !!!
$ git clone https://gitlab.com/python_431/1sysd23-b1b.git
~~~~
