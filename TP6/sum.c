#include<stdio.h>
#include<stdlib.h>

int main(int argc, char *argv[]) {
    double s = 0;

    for (int i = 1; i < argc; i++) {
        s += strtod(argv[i], NULL);
    }

    printf("%.2lf\n", s);
}
