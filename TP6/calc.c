#include<stdio.h>
#include<stdlib.h>

void usage(char *argv[]) {
    printf("%s val1 val2 op : Calcule val1 op val2 où op est +, -, * ou /\n", argv[0]);
    exit(EXIT_FAILURE); // code erreur différent de zéro
}

int main(int argc, char *argv[]) {
    double x, y;
    char op;

    if (argc != 4) { // argv est : ./calc 1 2 + (longueur 4)
        usage(argv);
    }

    x = strtod(argv[1], NULL);
    y = strtod(argv[2], NULL);
    op = argv[3][0]; // premier caractère du troisième argument

    switch (op) {
        case '+':
            printf("%.2lf\n", x + y);
            break;
        case '-':
            printf("%.2lf\n", x - y);
            break;
        case '/':
            printf("%.2lf\n", x / y);
            break;
        case '*':
            printf("%.2lf\n", x * y);
            break;
        default:
            usage(argv);
    }
    exit(EXIT_SUCCESS);
}
