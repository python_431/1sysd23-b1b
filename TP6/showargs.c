#include<stdio.h>

int main(int argc, char *argv[]) {
    // argc : arguments count (nb d'arguments, nom du programme inclu)
    // argv : arguments values : tableau de chaînes de longueur argc

    printf("argc = %d\n", argc);
    for (int i = 0; i < argc; i++) {
        printf("Argument : %d = %s\n", i, argv[i]);
    }
}
