#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void usage(char *argv[]) {
    printf("%s mot1 [mot2 ...] : trie ses arguments\n", argv[0]);
    exit(EXIT_FAILURE); // code erreur différent de zéro
}


void tri_tabstr(char *tab[], int n) {
   // s'inspirer du code dans TP3/tri.c
    char *tmp;
    int idx_min;
    for (int i = 0; i < n - 1; i++) {
        idx_min = i;
        for (int j = i + 1; j < n; j++) {
            if (strcmp(tab[j],tab[idx_min]) < 0) { // man 3 strcmp
                idx_min = j;
            }
        }
        if ( idx_min != i ) {
            tmp = tab[i];
            tab[i] = tab[idx_min];
            tab[idx_min] = tmp;
        }
    }
}

int main(int argc, char *argv[]) {
    char **t;

    if (argc == 1) {
        usage(argv);
    }

    t = malloc((argc - 1)*sizeof(char *));
    for (int i = 1; i < argc; i++) {
        t[i-1] = argv[i];
    }
    tri_tabstr(t, argc - 1);
    for (int i = 0; i < argc - 1; i++) {
        printf("%s\n", t[i]);
    }
    exit(EXIT_SUCCESS);
}
