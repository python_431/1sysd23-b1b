# Tableaux

`tab1.c` et `tab2.c` sont des solutions à l'exercice
de la page 33 du cours.

## Tri

Définir un tableau constant d'une douzaine de valeurs
de type `int` prédéfinies dans votre programme (`tri.c`) :

~~~~C
int numbers[] = { 42, 12, 15, 10, 5, 9, 2, 47, 88, 12, 66, 54 };
~~~~

Il existe plusieurs algorithmes de tri, parmi eux l'un des
plus simple (mais pas le plus efficace) est le _tri par
sélection_ : https://fr.wikipedia.org/wiki/Tri_par_s%C3%A9lection

Voici cet algorithme en pseudo-code :

~~~~
      n ← longueur(t) 
      pour i de 0 à n - 2
          idx_min ← i       
          pour j de i + 1 à n - 1
              si t[j] < t[idx_min], alors idx_min ← j
          fin pour
          si idx_min ≠ i, alors échanger t[i] et t[idx_min]
      fin pour
~~~~

Implémentez cet algorithme dans `tri.c` et affichez les
valeurs du tableau une fois trié.

Indices :

- Vous aurez besoin de deux boucles for imbriquées 
- Vous aurez besoin de permuter les valeurs de deux
éléments du tableau, comment faire ça ?

