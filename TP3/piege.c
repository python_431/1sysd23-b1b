#include<stdio.h>

// Un piège classique en C : se tromper entre
// entre = (affectation) et == (comparaison)

/*
Si on met une affection '=' au lieu d'une comparaison
dans un contexte ou a lieu un test (if, while, for)
c'est syntaxiquement valide en C. Parce que c'est
parfois utile, mais si c'est fait par erreur ça
conduit à du code toujours (boucle infinie, etc.)
ou jamais exécuté !

Les compilateurs modernes signalent _presque_ toujours
le problème potentiel avec un _warning_. 
*/

int main() {
    int i = 42;

    if ( i = 0 ) // que *vaut* ceci ? que *fait* ceci ?
                 // ça vaut 0. ça mets 0 dans i !!!!
                 // i.e. faux et i a changé
    // pour s'en défendre : 0 == i 
    {
        printf("i est zéro\n"); // ceci sera-t-il exécuté ?
    }
}


