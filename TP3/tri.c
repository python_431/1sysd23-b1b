#include<stdio.h>

int main() {
    int numbers[] = { 42, 12, 15, 10, 5, 9, 2, 47, 88, 12, 66, 54 };
    int n = 12;
    int idx_min, tmp;

    for (int i = 0; i < n; i++) {
        printf("%d ", numbers[i]);
    }
    printf("\n"); 
    // ici tri par sélection
    for (int i = 0; i < n - 1; i++) {
        idx_min = i;
        for (int j = i + 1; j < n; j++) {
            if (numbers[j] < numbers[idx_min]) {
                idx_min = j;
            }
        }
        if ( idx_min != i ) {
            tmp = numbers[i];
            numbers[i] = numbers[idx_min];
            numbers[idx_min] = tmp;
            printf("Permutation des éléments présent aux indices %d et %d : %d <-> %d\n", idx_min, i, numbers[idx_min], numbers[i]);
        } else {
            printf("Pas de permutation\n");
        }
    }
    // affichage du résultat
    for (int i = 0; i < n; i++) {
        printf("%d ", numbers[i]);
    }
    printf("\n");
}
