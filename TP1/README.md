# Ok Boomer !

Créer un programme C qui demande son âge à l'utilisateur et
affiche _Ok Boomer!_ s'il a plus de 42 ans et _Time to learn UNIX_
sinon...

On commence par créer le répertoire `TP1` à la base de notre
dépot et on y met le Makefile du TP0 :

~~~~Bash
$ cd ~/1sysd
$ pwd
/home/.../1sysd
$ mkdir TP1
$ cd TP1
$ cp ../TP0/Makefile .
$ git add Makefile
$ nano Makefile
(ou vim, vi, gedit, ..., mettre PROGS=boomer)
$ nano boomer.c
(ou vim, vi, gedit, ..., coder...)
$ make
$ ./boomer
$ git add boomer.c
$ git commit -a -m "exo fini"
$ git push
~~~~

# Tirer un nombre au hasard

Indice : il existe dans la bibliothèque C une fonction
`rand` qui renvoie un nombre "pseudo-aléatoire" entre
0 et RAND_MAX :

~~~~C
#include<stdio.h>
#include<stdlib.h>

int main() {
    int n;

    printf("RAND_MAX = %d\n", RAND_MAX);
    n = rand();
    printf("Tirage : %d\n", n);
    n = rand();
    printf("Tirage : %d\n", n);
    n = rand();
    printf("Tirage : %d\n", n);
    n = rand();
    printf("Tirage : %d\n", n);
}
~~~~

La séquence générée par rand() est toujours la même !

Pour que cette séquence ne soit plus prévisible il faut
commence dans le programme par appeler `srand()` avec
une graine _(seed)_ qui ne soit pas toujours la même.
Un candidat : le nombre de secondes écoulées depuis
le 1er janvier 1970 à 00h00 (l'epoch). On peut
l'obtenir avec `time(NULL)`.

On peut ramener l'intervalle `[0, RAND_MAX]` à 
`[1, 10]` (par exemple) avec l'opération modulo 
(reste de la division euclidienne).

Conclusion : pour obtenir un nombre raisonnablement
aléatoire entre 1 et 10 :

~~~~C
    srand(time(NULL));  // graine (au début de main)
    ...
    n = rand() % 10 + 1;
~~~~

Écrire un programme `game1` qui tire un nombre au hasard entre
1 et 10, demande à l'utilisateur de le deviner et lui
dit s'il l'a trouvé ou non et affiche la valeur tirée
ensuite.


