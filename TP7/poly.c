#include<stdio.h>
#include <sys/param.h>

struct Poly {
    int degree;
    double coeffs[50]; // deg max 49
};

// fonction power (pas besoin de pow qui élève à une puissance
// non entière

double power(double x, int n) {
    double result = 1;
    for (int i = 0; i < n; i++) {
        result *= x;
    }
    return result;
}


// TODO : traiter plus finement les coefficients nuls, négatifs, etc.
void pdisplay(struct Poly p) {
    for (int i = p.degree; i >= 0; i--) {
        switch (i) {
            case 0:
                printf("(%.2lf) ", p.coeffs[i]);
                break;
            case 1:
                printf("(%.2lf)x ", p.coeffs[i]);
                break; 
            default:     
                printf("(%.2lf)x^%d ", p.coeffs[i], i);
        }
        if ( i > 0 ) { // pas le dernier terme
            printf("+ ");
        }
    }
    printf("\n");
}

double pvalue(struct Poly p, double x) {
    double result = 0;
    for (int i = 0; i <= p.degree; i++) {
        result += p.coeffs[i]*power(x, i);
    }
    return result;
}

struct Poly padd(struct Poly p1, struct Poly p2) {
    int deg_max = MAX(p1.degree, p2.degree);
    int deg_min = MIN(p1.degree, p2.degree);
    struct Poly p3;
    p2.degree = deg_max;
    for ( int i = 0; i <= deg_max; i++ ) {
        if ( i <= deg_min + 1 ) {
            p3.coeffs[i] = p1.coeffs[i] + p2.coeffs[i];
        }
    }
}

int main() {
    struct Poly p1 = { 2, { 3, 2, 1 } };     // x² + 2x + 3
    struct Poly p2 = { 3, { 1, 0, -1, 2 } }; // 2x^3 - x^2 + 1
    // -x^7 + 8 x^6 + 3x^5 + x^4 + x^2 + 3x + 2
    struct Poly p3 = { 7, { 2, 3, 1, 0, 1, 3, 8, -1} };

    printf("5^0 = %.2lf\n", power(5, 0));
    printf("5^1 = %.2lf\n", power(5, 1));
    printf("5^2 = %.2lf\n", power(5, 2));
    printf("5^3 = %.2lf\n", power(5, 3));

    pdisplay(p1);
    pdisplay(p2);
    pdisplay(p3);
    printf("p2(2) = %.2lf\n", pvalue(p2, 2));

}
