# Un jeu plus intéressant

Écrire un programme (source `game.c`) qui tire un nombre
au hasard entre 1 et 100. Et demande à l'utilisateur
une proposition. Si le joueur gagne on le félicite et
on affiche le nombre de tentatives. Sinon on lui dit
"plus grand" ou "plus petit" et on lui redemande une
proposition, jusqu'à ce qu'il trouve.

Un point de départ possible est :

~~~~C
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int main() {
    int found = 0; // vaudra 1 si trouvé
    int secret, guess, count = 0;
    srand(time(NULL)); // initialisation hasard
    secret = rand() % 100 + 1; // tirage
    printf("J'ai tiré un nombre entre 1 et 100\n");
    while (!found) {
        printf("Votre proposition : ");
        scanf("%d", &guess);
        count++;
        if ( ) { // égal ?

        } else if ( ) { // plus grand ?

        } else { // reste un cas : plus petit...

        }
    }
}
~~~~
